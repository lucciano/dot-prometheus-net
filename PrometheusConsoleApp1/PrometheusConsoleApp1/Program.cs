﻿using Prometheus;
using System;
using System.Threading;

namespace PrometheusConsoleApp1
{
class Program
{
        private static readonly Counter TickTock =
        Metrics.CreateCounter("sampleapp_ticks_total", "Just keeps on ticking");

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            // var server = new MetricServer(hostname: "localhost", port: 3091);
            // server.Start();
            var pusher = new MetricPusher(new MetricPusherOptions
            {
                Endpoint = "http://192.168.2.90:3091/metrics",
                Job = "PrometheusConsoleApp1"
            });
            pusher.Start();

            while (true)
            {
                TickTock.Inc();
                Thread.Sleep(TimeSpan.FromSeconds(1));
            }
        }
    }
}
